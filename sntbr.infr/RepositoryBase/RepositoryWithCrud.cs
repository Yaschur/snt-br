﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace sntbr.infr.RepositoryBase
{
	public class RepositoryWithCrud<TEntity, TKey>
	{
		public RepositoryWithCrud(IStore store, Expression<Func<TEntity, TKey>> primaryKeyExpression)
		{
			_store = store;
			_primaryKeyExpr = primaryKeyExpression;
			_primaryKeyFunc = primaryKeyExpression.Compile();
		}

		public void Store(TEntity entity)
		{
			_store.Store(entity);
		}

		public void Remove(TEntity entity)
		{
			_store.Remove<TEntity>(_primaryKeyFunc.Invoke(entity));
		}

		public IQueryable<TEntity> FindAll()
		{
			return _store.AllOf<TEntity>();
		}

		public TEntity GetById(TKey entityId)
		{
			Expression<Func<TEntity, bool>> eqExpr = Expression.Lambda<Func<TEntity, bool>>(
				Expression.Equal(_primaryKeyExpr.Body, Expression.Constant(entityId)),
				_primaryKeyExpr.Parameters
				);
			return FindAll().FirstOrDefault(eqExpr);
		}

		protected readonly Expression<Func<TEntity, TKey>> _primaryKeyExpr;
		protected readonly Func<TEntity, TKey> _primaryKeyFunc;
		protected readonly IStore _store;
	}
}
