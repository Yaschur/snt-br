﻿using System.Collections.Generic;
using sntbr.front.Models.Profile.Partials;

namespace sntbr.front.Models.Profile.Pages
{
	public class SitePage : PageBaseViewModel
	{
		public IEnumerable<SiteEditPartial> Sites { get; private set; }

		public SitePage(IEnumerable<SiteEditPartial> sites)
		{
			Sites = sites;
		}
	}
}