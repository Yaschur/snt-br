﻿using System.Linq;
using sntbr.domain.Publications;
using sntbr.domain.Publications.Repositories;
using sntbr.front.Models.Manage.Pages;
using sntbr.front.Models.Manage.Partials;


namespace sntbr.front.Models.Manage
{
	public class ManagePageFactory : IPageService
	{
		public ManagePageFactory(ArticleRepository articleRepository)
		{
			_articleRepo = articleRepository;
		}

		public IndexPage IndexPage()
		{
			ArticlePartial[] articles = _articleRepo.FindAll()
				.OrderByDescending(a => a.IsPublished)
				.ThenByDescending(a => a.PublishedAt)
				.Select(a => new ArticlePartial(a))
				.ToArray();
			return new IndexPage(articles);
		}

		public EditPage EditPage(string id = "")
		{
			ArticleEditModel editModel = new ArticleEditModel();
			Article article = null;
			bool isNew = string.IsNullOrEmpty(id);
			if (!isNew)
				article = _articleRepo.GetById(id);
			if (!isNew && article == null)
				return null;
			if (article != null)
			{
				editModel.Title = article.Title;
				editModel.Story = article.Story;
			}

			return new EditPage(editModel);
		}

		public EditPage EditPage(ArticleEditModel inputModel)
		{
			return new EditPage(inputModel);
		}

		private readonly ArticleRepository _articleRepo;
	}
}