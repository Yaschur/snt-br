﻿using System.Collections.Generic;
using sntbr.front.Models.Account.Partial;

namespace sntbr.front.Models.Account.Pages
{
	public class DetailsPage : PageBaseViewModel
	{
		public AppUserEditModel User { get; private set; }
		public IEnumerable<string> AllRoles { get; private set; }

		public DetailsPage(AppUserEditModel user, IEnumerable<string> allRoles)
			: base()
		{
			User = user;
			AllRoles = allRoles;
		}
	}
}