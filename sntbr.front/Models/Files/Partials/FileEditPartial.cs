﻿
namespace sntbr.front.Models.Files.Partials
{
	public class FileEditPartial
	{
		public string Title { get; set; }
		public string Description { get; set; }
	}
}