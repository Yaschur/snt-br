﻿using System.Net;
using System.Web.Mvc;
using sntbr.front.Models.Common;
using sntbr.front.Models.Housing;
using sntbr.front.Models.Housing.Pages;

namespace sntbr.front.Controllers
{
	[Authorize(Roles = SntbrRoles.Administrator)]
	public class HousingController : Controller
	{
		public HousingController(HousingPageFactory pageFactory, HousingInputService inputService)
		{
			_pageFactory = pageFactory;
			_inputService = inputService;
		}

		public ActionResult Index()
		{
			IndexPage page = _pageFactory.IndexPage();

			return View("~/Views/Housing/Pages/Index.cshtml", page);
		}

		public ActionResult Create()
		{
			EditPage page = _pageFactory.EditPage();

			return View("~/Views/Housing/Pages/Edit.cshtml", page);
		}

		[HttpPost]
		public ActionResult Create(string number)
		{
			_inputService.ProcessCreate(number);

			return RedirectToAction("index");
		}

		public ActionResult Edit(string id)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			EditPage page = _pageFactory.EditPage(id);
			if (page == null)
				return new HttpStatusCodeResult(HttpStatusCode.NotFound);

			return View("~/Views/Housing/Pages/Edit.cshtml", page);
		}

		[HttpPost]
		public ActionResult Edit(string id, string number)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			_inputService.ProcessEdit(number, id);

			return RedirectToAction("index");
		}

		private readonly HousingPageFactory _pageFactory;
		private readonly HousingInputService _inputService;
	}
}