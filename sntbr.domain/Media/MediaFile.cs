﻿using System;

namespace sntbr.domain.Media
{
	public class MediaFile
	{
		public string FileName { get; private set; }
		public string OriginalFileName { get; private set; }
		public string Title { get; private set; }
		public string Description { get; private set; }

		public MediaFile(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentException("Cannot be null or empty", "name");

			FileName = name;
			OriginalFileName = name;
		}

		public void Rename(string name, bool renameOriginalName = false)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentException("Cannot be null or empty", "name");
			if (renameOriginalName)
			{
				OriginalFileName = name;
				return;
			}
			FileName = name;
		}

		public void Describe(string title, string description)
		{
			if (string.IsNullOrWhiteSpace(title))
				throw new ArgumentException("Cannot be null or empty", "title");

			Title = title;
			Description = description;
		}
	}
}
