﻿
namespace sntbr.front.Models.Account.Partial
{
	public class AppUserEditModel
	{
		public string UserName { get; set; }
		public string Email { get; set; }

		public string[] InRoles { get; set; }
	}
}