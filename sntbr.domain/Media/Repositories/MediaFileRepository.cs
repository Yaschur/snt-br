﻿using System.Linq;
using sntbr.infr;
using sntbr.infr.RepositoryBase;

namespace sntbr.domain.Media.Repositories
{
	public class MediaFileRepository : RepositoryWithMap<MediaFile, string>
	{
		public MediaFileRepository(IStore store)
			: base(store, mf => mf.FileName)
		{ }

		public IQueryable<TMedia> FindAllOf<TMedia>()
			where TMedia : MediaFile
		{
			return _store.AllOf<TMedia, MediaFile>();
		}

		protected override void MapToStore(IStoreMapper<MediaFile> storeMapper)
		{
			storeMapper.SetMap<ImageFile>();
			storeMapper.SetKey(mf => mf.FileName);
			return;
		}
	}
}
