﻿using System;

namespace sntbr.domain.Housing
{
	public class Resident
	{
		public string Id { get; private set; }

		public string UserName { get; private set; }

		public string LastName { get; private set; }

		public string FirstName { get; private set; }

		public string MiddleName { get; private set; }

		public Resident(string id)
			: this(id, null, null, null)
		{ }

		public Resident(string id, string lastName, string firstName, string middleName)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");
			Id = id;
			SetName(lastName, firstName, middleName);
		}

		public void SetName(string lastName, string firstName, string middleName)
		{
			LastName = lastName;
			FirstName = firstName;
			MiddleName = middleName;
		}

		public void AssociateUser(string userName)
		{
			UserName = userName;
		}
	}
}
