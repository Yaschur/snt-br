﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using sntbr.front.AppBuild;

namespace sntbr.front.App_Start
{
	public class DependencyConfig
	{
		public static void RegisterResolver()
		{
			IWindsorContainer container = new WindsorContainer();
			container.Install(FromAssembly.This());
			DependencyResolver.SetResolver(new CastleDependencyResolver(container));
		}
	}
}