﻿using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace sntbr.infr.MongoStore
{
	public class MongoStore : IStore
	{
		public MongoStore(string connectionString)
		{
			MongoUrl mUrl = new MongoUrl(connectionString);
			MongoClient mClient = new MongoClient(mUrl);
			_server = mClient.GetServer();
			_db = _server.GetDatabase(mUrl.DatabaseName);
		}

		public void Store<T>(T objectToSave)
		{
			GetMongoCollection<T>().Save(objectToSave);
		}

		public void Remove<T>(object id)
		{
			IMongoQuery q = Query.EQ("_id", BsonValue.Create(id));
			GetMongoCollection<T>().Remove(q);
		}

		public IQueryable<T> AllOf<T>()
		{
			return GetMongoCollection<T>().AsQueryable();
		}

		public IQueryable<TDerived> AllOf<TDerived, TParent>()
			where TDerived : TParent
		{
			string derivedTypeName = typeof(TDerived).Name;
			IMongoQuery q = Query.EQ("_t", derivedTypeName);
			return GetMongoCollection<TParent>().Find(q).Cast<TDerived>().AsQueryable();
		}

		public IStoreMapper<T> GetMapper<T>()
		{
			return new MongoStoreMapper<T>();
		}

		public void Dispose()
		{
			return;
		}

		private MongoCollection<T> GetMongoCollection<T>()
		{
			string collectionName = typeof(T).Name.ToLower();
			return _db.GetCollection<T>(collectionName);
		}

		private readonly MongoServer _server;
		private readonly MongoDatabase _db;
	}
}
