﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;
using sntbr.front.Models.Configuration;

namespace sntbr.front.App_Start
{
	public class OwinConfig
	{
		public static void Configure(IAppBuilder app)
		{
			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString("/Account/Login")
			});

			app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


			if (SntbrSettings.Configuration.GoogleCreds != null)
				app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
				{
					ClientId = SntbrSettings.Configuration.GoogleCreds.Key,
					ClientSecret = SntbrSettings.Configuration.GoogleCreds.Secret
				});
			if (SntbrSettings.Configuration.FacebookCreds != null)
				app.UseFacebookAuthentication(new FacebookAuthenticationOptions()
				{
					AppId = SntbrSettings.Configuration.FacebookCreds.Key,
					AppSecret = SntbrSettings.Configuration.FacebookCreds.Secret
				});
		}
	}
}