﻿using System.Collections.Generic;
using System.Linq;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;
using sntbr.front.Models.Housing.Pages;
using sntbr.front.Models.Housing.Partials;

namespace sntbr.front.Models.Housing
{
	public class HousingPageFactory : IPageService
	{
		public HousingPageFactory(SiteRepository siteRepository, ResidentRepository residentRepository)
		{
			_siteRepo = siteRepository;
			_residentRepo = residentRepository;
		}

		public IndexPage IndexPage()
		{
			List<Site> sites = _siteRepo.FindAll()
				.ToList()
				.OrderBy(s => s.Number.PadLeft(2, '0'))
				.ToList();
			List<Resident> residents = _residentRepo.FindAll().ToList();

			SitePartial[] sPartials = sites.Select(s =>
				new SitePartial(
					s.Id,
					s.Number,
					residents
					.Where(r => s.HasResidence(r))
					.Select(r => new ResidentInSitePartial
					{
						ResidentName = (r.FirstName ?? "") + " " + (r.LastName ?? ""),
						UserName = r.UserName
					}).ToArray()
				)
			).ToArray();


			IndexPage page = new IndexPage(sPartials);

			return page;
		}

		public EditPage EditPage(string id = "")
		{
			string number = "";
			if (!string.IsNullOrEmpty(id))
			{
				Site site = _siteRepo.GetById(id);
				number = site.Number;
			}

			EditPage page = new EditPage(number);

			return page;
		}

		private readonly SiteRepository _siteRepo;
		private readonly ResidentRepository _residentRepo;
	}
}