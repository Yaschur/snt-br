﻿using System.Linq;
using sntbr.domain.Media;
using sntbr.domain.Media.Repositories;
using sntbr.front.Models.Files.Pages;
using sntbr.front.Models.Files.Partials;

namespace sntbr.front.Models.Files
{
	public class FilesPageFactory : IPageService
	{
		public const string ContentFolderPath = "content/files/";

		public FilesPageFactory(MediaFileRepository mediaFileRepository)
		{
			_mediaFileRepository = mediaFileRepository;
		}

		public IndexPage IndexPage()
		{
			FilePartial[] files = _mediaFileRepository.FindAll()
				.OrderBy(f => f.Title)
				.ThenBy(f => f.FileName)
				.Select(f => new FilePartial
				{
					Description = f.Description,
					Title = f.Title,
					Url = GetUrl(f.FileName)
				})
				.ToArray();

			IndexPage page = new IndexPage(files);

			return page;
		}

		public UploadPage UploadPage(string id = "")
		{
			FileEditPartial fileDesc = new FileEditPartial();
			MediaFile mediaFile = null;
			bool isNew = string.IsNullOrEmpty(id);
			if (!isNew)
				mediaFile = _mediaFileRepository.GetById(id);
			if (!isNew && mediaFile == null)
				return null;
			if (mediaFile != null)
			{
				fileDesc.Title = mediaFile.Title;
				fileDesc.Description = mediaFile.Description;
			}
			return new UploadPage(fileDesc);
		}

		private string GetUrl(string fileName)
		{
			return string.Format("/{0}{1}", ContentFolderPath, fileName);
		}

		//private readonly MediaFolder _mediaFolder;
		private readonly MediaFileRepository _mediaFileRepository;
	}
}