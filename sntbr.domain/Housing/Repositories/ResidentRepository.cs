﻿using System.Collections.Generic;
using System.Linq;
using sntbr.infr;
using sntbr.infr.RepositoryBase;

namespace sntbr.domain.Housing.Repositories
{
	public class ResidentRepository : RepositoryWithMap<Resident, string>
	{
		public ResidentRepository(IStore store)
			: base(store, r => r.Id)
		{ }

		public IQueryable<Resident> FindByIds(IEnumerable<string> ids)
		{
			return FindAll()
				.Where(r => ids.Contains(r.Id));
		}

		public Resident GetByUserName(string userName)
		{
			return FindAll().FirstOrDefault(s => s.UserName == userName);
		}

		protected override void MapToStore(IStoreMapper<Resident> storeMapper)
		{
			return;
		}
	}
}
