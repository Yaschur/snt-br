﻿using System.Web.Mvc;
using sntbr.front.Models.Common;
using sntbr.front.Models.Profile;
using sntbr.front.Models.Profile.Pages;
using sntbr.front.Models.Profile.Partials;

namespace sntbr.front.Controllers
{
	[Authorize(Roles = SntbrRoles.User)]
	public class ProfileController : Controller
	{
		public ProfileController(ProfilePageFactory pageFactory, ProfileInputService inputService)
		{
			_pageFactory = pageFactory;
			_inputService = inputService;
		}

		public ActionResult Index()
		{
			IndexPage page = _pageFactory.IndexPage(User.Identity.Name);

			return View("~/Views/Profile/Pages/Index.cshtml", page);
		}

		public ActionResult Edit()
		{
			EditPage page = _pageFactory.EditPage(User.Identity.Name);

			return View("~/Views/Profile/Pages/Edit.cshtml", page);
		}

		[HttpPost]
		public ActionResult Edit(ProfileEditModel model)
		{
			if (ModelState.IsValid)
			{
				_inputService.ProcessProfileEdit(User.Identity.Name, model);
				return RedirectToAction("index");
			}

			EditPage page = _pageFactory.EditPage(model);
			return View("~/Views/Profile/Pages/Edit.cshtml", page);
		}

		public ActionResult Site()
		{
			SitePage page = _pageFactory.SitePage(User.Identity.Name);
			if (page == null)
				return RedirectToAction("edit");
			return View("~/Views/Profile/Pages/Site.cshtml", page);
		}

		[HttpPost]
		public ActionResult Site(string action, string site)
		{
			_inputService.ProcessSiteSet(User.Identity.Name, site, action);

			return RedirectToAction("index");
		}

		private readonly ProfilePageFactory _pageFactory;
		private readonly ProfileInputService _inputService;
	}
}