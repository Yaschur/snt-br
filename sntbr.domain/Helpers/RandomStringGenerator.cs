﻿using System;
using System.Linq;

namespace sntbr.domain.Helpers
{
	public class RandomStringGenerator
	{
		public static string GetRandomString(int size)
		{
			char[] str = Enumerable.Range(0, Math.Abs(size))
				.Select(x => alphaNums[random.Next(0, alphaNums.Length)])
				.ToArray();
			return new string(str);
		}

		private static Random random = new Random();
		private const string alphaNums = "abcdefghijklmnopqrstuvwxyz0123456789";
	}
}
