﻿
namespace sntbr.front.Models.Profile.Pages
{
	public class IndexPage : PageBaseViewModel
	{
		public string FullName { get; private set; }
		public string Email { get; private set; }
		public string RolesInfo { get; private set; }
		public string SitesInfo { get; private set; }

		public IndexPage(string fullName, string email, string rolesInfo, string sitesInfo)
			: base()
		{
			FullName = fullName;
			Email = email;
			RolesInfo = rolesInfo;
			SitesInfo = sitesInfo;
		}
	}
}