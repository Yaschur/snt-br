﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace sntbr.infr.MongoIdentity
{
	public class IdentityUser : IUser
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		public string UserName { get; set; }

		public List<string> Roles { get; private set; }

		public List<UserLoginInfo> Logins { get; private set; }

		public string Email { get; set; }

		public IdentityUser()
		{
			Roles = new List<string>();
			Logins = new List<UserLoginInfo>();
		}

		public IdentityUser(string userName)
			: this()
		{
			UserName = userName;
		}
	}
}
