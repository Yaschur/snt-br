﻿namespace sntbr.front.Models
{
	public interface IWidgetViewModel
	{
		string PartialView { get; }
	}
}
