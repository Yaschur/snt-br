﻿using System;
using System.Linq.Expressions;

namespace sntbr.infr.RepositoryBase
{
	public abstract class RepositoryWithMap<TEntity, TKey> : RepositoryWithCrud<TEntity, TKey>
	{
		public RepositoryWithMap(IStore store, Expression<Func<TEntity, TKey>> primaryKeyExpression)
			: base(store, primaryKeyExpression)
		{
			InitStoreMapping();
		}

		private void InitStoreMapping()
		{
			IStoreMapper<TEntity> sm = _store.GetMapper<TEntity>();
			if (sm == null || sm.AlreadyMapped)
				return;
			sm.SetKey(_primaryKeyExpr);
			MapToStore(sm);
		}

		protected abstract void MapToStore(IStoreMapper<TEntity> storeMapper);
	}
}
