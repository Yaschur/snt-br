﻿using System.Collections.Generic;
using sntbr.front.Models.Manage.Partials;

namespace sntbr.front.Models.Manage.Pages
{
	public class IndexPage : PageBaseViewModel
	{
		public IEnumerable<ArticlePartial> Articles { get; private set; }

		public IndexPage(IEnumerable<ArticlePartial> articles)
			: base()
		{
			Articles = articles;
		}
	}
}