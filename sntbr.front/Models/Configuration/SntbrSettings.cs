﻿
using System.IO;
using Newtonsoft.Json;
namespace sntbr.front.Models.Configuration
{
	public class SntbrSettings
	{
		public string ConnectionString { get; set; }
		public ProviderPair GoogleCreds { get; set; }
		public ProviderPair FacebookCreds { get; set; }

		public bool NoInitialConfiguration { get { return string.IsNullOrEmpty(ConnectionString); } }

		#region Singletone static stuff
		public static SntbrSettings Configuration
		{
			get
			{
				if (_config == null)
					_config = LoadConfiguration();
				return _config;
			}
			set
			{
				_config = value;
				SaveConfiguration();
			}
		}

		public static void Init(string configurationFilePath)
		{
			_configFilePath = configurationFilePath;
		}

		private static string _configFilePath;
		private static SntbrSettings _config;

		private static SntbrSettings LoadConfiguration()
		{
			return File.Exists(_configFilePath) ?
				JsonConvert.DeserializeObject<SntbrSettings>(File.ReadAllText(_configFilePath))
				: new SntbrSettings();
		}

		private static void SaveConfiguration()
		{
			File.WriteAllText(_configFilePath, JsonConvert.SerializeObject(_config));
		}

		#endregion

		public class ProviderPair
		{
			public string Key { get; private set; }
			public string Secret { get; private set; }

			public ProviderPair(string key, string secret)
			{
				Key = key;
				Secret = secret;
			}
		}
	}
}