﻿using System;

namespace sntbr.front.Models.Widgets
{
	public class AuthWidget : IWidgetViewModel
	{
		public string Name { get; private set; }

		public string PartialView
		{
			get { return "~/Views/Widgets/AuthWidget.cshtml"; }
		}

		public AuthWidget(string name)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");
			Name = name;
		}
	}
}