﻿using sntbr.domain.Helpers;
using sntbr.domain.Publications;

namespace sntbr.front.Models.Home.Partials
{
	public class ArticlePartial
	{
		public string Title { get; set; }
		public string Story { get; set; }
		public string CreatedAt { get; set; }
		public string ChangedAt { get; set; }
		public string PublishedAt { get; set; }

		public ArticlePartial(Article article)
		{
			Title = article.Title;
			Story = article.Story;
			CreatedAt = article.CreatedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm");
			ChangedAt = article.ChangedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm");
			PublishedAt = article.PublishedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm");
		}
	}
}