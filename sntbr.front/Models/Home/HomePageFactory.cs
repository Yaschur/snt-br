﻿using System;
using System.Linq;
using sntbr.domain.Publications.Repositories;
using sntbr.front.Models.Home.Pages;
using sntbr.front.Models.Home.Partials;
using sntbr.front.Models.Widgets;

namespace sntbr.front.Models.Home
{
	public class HomePageFactory : PageFactoryBase, IPageService
	{
		public HomePageFactory(ArticleRepository articleRepository, WidgetFactory widgetFactory)
			: base(widgetFactory)
		{
			if (articleRepository == null)
				throw new ArgumentNullException("articleRepository");
			_articleRepo = articleRepository;
		}

		public IndexPage IndexPage()
		{
			var articles = _articleRepo.FindPublished()
				.OrderByDescending(a => a.PublishedAt)
				.Select(a => new ArticlePartial(a))
				.ToList();

			IndexPage page = new IndexPage(articles);

			page.SetWidgets(
				_widgetFactory.GisMeteoWidget(),
				_widgetFactory.AuthWidget()
			);

			return page;
		}

		private readonly ArticleRepository _articleRepo;
	}
}