﻿
using sntbr.front.Models.Common;
namespace sntbr.front.Models
{
	public abstract class PageBaseViewModel
	{
		public string Title { get; private set; }
		public string Description { get; private set; }

		public IWidgetViewModel[] Widgets { get; private set; }

		public PageBaseViewModel(string title = null, string description = null)
		{
			Title = title ?? Constants.GeneralTitle;
			Description = description ?? Constants.GeneralDescription;
			Widgets = new IWidgetViewModel[0];
		}

		public void SetWidgets(params IWidgetViewModel[] widgets)
		{
			Widgets = widgets;
		}
	}
}