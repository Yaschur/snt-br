﻿using System.Linq;
using sntbr.infr;
using sntbr.infr.RepositoryBase;

namespace sntbr.domain.Housing.Repositories
{
	public class SiteRepository : RepositoryWithMap<Site, string>
	{
		public SiteRepository(IStore store)
			: base(store, s => s.Id)
		{ }

		public IQueryable<Site> FindByNumber(string number)
		{
			return FindAll().Where(s => s.Number.Contains(number));
		}

		public Site GetByNumber(string number)
		{
			return FindAll().FirstOrDefault(s => s.Number == number);
		}

		public IQueryable<Site> FindByResidence(string residentId)
		{
			// HACK: memory
			return FindAll().ToList().Where(s => s.HasResidence(residentId)).AsQueryable();
		}

		protected override void MapToStore(IStoreMapper<Site> storeMapper)
		{
			storeMapper.SetMap("_residents");
		}
	}
}
