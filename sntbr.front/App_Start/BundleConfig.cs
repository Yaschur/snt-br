﻿using System.Web.Optimization;

namespace sntbr.front
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			//bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
			//			"~/Scripts/jquery-{version}.js"));

			//bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
			//			"~/Scripts/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			//bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
			//			"~/Scripts/modernizr-*"));

			//bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
			//		  "~/Scripts/bootstrap.js",
			//		  "~/Scripts/respond.js"));

			//bundles.Add(new ScriptBundle("~/bundles/Scripts/ckeditor").Include(
			//	"~/Scripts/ckeditor/ckeditor.js")
			//);

			bundles.Add(
				new StyleBundle("~/Content/site")
				.Include(
					"~/Content/css/normalize.css",
					"~/Content/css/site.css")
			);

			bundles.Add(
				new StyleBundle("~/Content/back")
				.Include(
					"~/Content/css/normalize.css",
					"~/Content/css/back.css")
			);

			bundles.Add(
				new StyleBundle("~/Content/front")
				.Include(
				"~/Content/css/normalize.css",
				"~/Content/css/front.css")
			);

			bundles.Add(
				new StyleBundle("~/Content/login")
				.Include(
				"~/Content/css/normalize.css",
				"~/Content/css/login.css")
			);

			// Set EnableOptimizations to false for debugging. For more information,
			// visit http://go.microsoft.com/fwlink/?LinkId=301862
			//BundleTable.EnableOptimizations = true;
		}
	}
}
