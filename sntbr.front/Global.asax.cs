﻿using System;
using System.Web.Optimization;
using System.Web.Routing;
using sntbr.front.App_Start;
using sntbr.front.Models.Configuration;

namespace sntbr.front
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			// App settings
			SntbrSettings.Init(Server.MapPath("~/App_Data/config.json"));
			if (SntbrSettings.Configuration.NoInitialConfiguration)
				throw new ApplicationException("No configuration... so bad is coming your way...");
			//--

			//AreaRegistration.RegisterAllAreas();
			//FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			DependencyConfig.RegisterResolver();
		}
	}
}
