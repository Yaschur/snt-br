﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using sntbr.front.Models.Account;
using sntbr.front.Models.Account.Pages;
using sntbr.front.Models.Common;
using sntbr.front.Services;

namespace sntbr.front.Controllers
{
	public class AccountController : Controller
	{
		public AccountController(AccountPageFactory pageFactory, AccountInputService inputService)
		{
			_pageFactory = pageFactory;
			_inputService = inputService;
		}

		public ActionResult Login(string returnUrl)
		{
			if (User.Identity.IsAuthenticated)
				return RedirectToAction("Index", "Home");
			LoginPage pageModel = _pageFactory.LoginPage(returnUrl);
			return View("~/Views/Account/Pages/Login.cshtml", pageModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			_inputService.ProcessLogoff();
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ExternalLogin(string provider, string returnUrl)
		{
			// Request a redirect to the external login provider
			return _pageFactory.ExternalLogin(
				provider,
				Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl })
				);
		}

		public ActionResult ExternalLoginCallback(string returnUrl)
		{
			IEnumerable<string> errors = null;

			_inputService.ProcessExternalCallBackAsync(false, out errors);

			if (errors.Any())
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, string.Join(" | ", errors));

			return RedirectToLocal(returnUrl);
		}

		[Authorize(Roles = SntbrRoles.Administrator)]
		public ActionResult Users()
		{
			UsersPage pageModel = _pageFactory.UsersPage();
			return View("~/Views/Account/Pages/Users.cshtml", pageModel);
		}

		[Authorize(Roles = SntbrRoles.Administrator)]
		public ActionResult Details(string id)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			DetailsPage pageModel = _pageFactory.DetailsPage(id);

			if (pageModel == null)
				return new HttpStatusCodeResult(HttpStatusCode.NotFound);

			return View("~/Views/Account/Pages/Details.cshtml", pageModel);
		}

		[Authorize(Roles = SntbrRoles.Administrator)]
		[HttpPost]
		public ActionResult Details(string id, string[] inroles)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			_inputService.ProcessDetailsEdit(id, inroles);
			return RedirectToAction("Users");
		}

		private readonly AccountPageFactory _pageFactory;
		private readonly AccountInputService _inputService;

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Index", "Home");
			}
		}
	}
}