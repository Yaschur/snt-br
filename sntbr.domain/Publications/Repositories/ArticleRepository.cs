﻿using System.Linq;
using sntbr.infr;
using sntbr.infr.RepositoryBase;

namespace sntbr.domain.Publications.Repositories
{
	public class ArticleRepository : RepositoryWithMap<Article, string>
	{
		public ArticleRepository(IStore store)
			: base(store, a => a.Id)
		{
		}

		public IQueryable<Article> FindPublished()
		{
			return FindAll().Where(a => a.IsPublished);
		}

		protected override void MapToStore(IStoreMapper<Article> storeMapper)
		{
			return;
		}
	}
}
