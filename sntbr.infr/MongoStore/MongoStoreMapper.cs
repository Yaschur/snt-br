﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using MongoDB.Bson.Serialization;

namespace sntbr.infr.MongoStore
{
	public class MongoStoreMapper<T> : IStoreMapper<T>
	{
		public MongoStoreMapper()
		{
			_already = BsonClassMap.IsClassMapRegistered(typeof(T));
			if (_already)
				return;
			_cm = BsonClassMap.RegisterClassMap<T>();
		}

		public void SetMap<TDerived>()
		{
			BsonClassMap.RegisterClassMap<TDerived>();
		}

		public void SetMap<TProp>(Expression<Func<T, TProp>> lambdaExpression)
		{
			if (_already)
				return;
			_cm.MapMember(lambdaExpression);
		}

		public void SetMap(string fieldName)
		{
			if (_already)
				return;
			MemberInfo[] mis = typeof(T).GetMember(fieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			if (mis == null || mis.Length == 0)
				throw new ApplicationException(string.Format("Can't find '{0}' member to map in store...", fieldName));
			if (mis.Length > 1)
				throw new ApplicationException(string.Format("Too many matching for '{0}' member to map in store...", fieldName));
			_cm.MapMember(mis[0]);
		}

		public void SetIgnore<TProp>(Expression<Func<T, TProp>> lambdaExpression)
		{
			if (_already)
				return;
			_cm.UnmapMember(lambdaExpression);
		}

		public void SetKey<TProp>(Expression<Func<T, TProp>> lambdaExpression)
		{
			if (_already)
				return;
			_cm.SetIdMember(_cm.GetMemberMap(lambdaExpression));
		}

		public bool AlreadyMapped { get { return _already; } }

		private readonly BsonClassMap<T> _cm;
		private readonly bool _already;
	}
}
