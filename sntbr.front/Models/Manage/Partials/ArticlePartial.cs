﻿using sntbr.domain.Helpers;
using sntbr.domain.Publications;

namespace sntbr.front.Models.Manage.Partials
{
	public class ArticlePartial
	{
		public string Id { get; set; }
		public string Title { get; set; }
		public string Story { get; set; }
		public string CreatedAt { get; set; }
		public string ChangedAt { get; set; }
		public string PublishedAt { get; set; }
		public string PublishCommandName { get; set; }
		public bool Unpublished { get; set; }

		public ArticlePartial(Article article)
		{
			Id = article.Id;
			Title = article.Title;
			Story = article.Story;
			CreatedAt = article.CreatedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm");
			ChangedAt = article.ChangedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm");
			PublishedAt = article.IsPublished ? article.PublishedAt.ToMoscowTime().ToString("dd/MM/yyyy HH:mm")
				: "НЕ ОПУБЛИКОВАНО";
			Unpublished = !article.IsPublished;
			PublishCommandName = article.IsPublished ? "снять с публикации" : "опубликовать";
		}
	}
}