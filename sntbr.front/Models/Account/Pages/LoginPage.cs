﻿using System.Collections.Generic;
using sntbr.front.Models.Account.Partials;

namespace sntbr.front.Models.Account.Pages
{
	public class LoginPage : PageBaseViewModel
	{
		public IEnumerable<ProviderPartial> Providers { get; private set; }
		public string ReturnUrl { get; private set; }

		public LoginPage(IEnumerable<ProviderPartial> providers, string returnUrl)
			: base()
		{
			Providers = providers;
			ReturnUrl = returnUrl;
		}
	}
}