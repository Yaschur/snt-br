﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace sntbr.front.Models.Manage.Partials
{
	public class ArticleEditModel
	{
		[Required]
		[StringLength(50)]
		[Display(Name = "заголовок объявления")]
		public string Title { get; set; }
		[Required]
		[AllowHtml]
		[Display(Name = "текст объявления")]
		public string Story { get; set; }
	}
}