﻿
namespace sntbr.front.Models.Housing.Partials
{
	public class ResidentInSitePartial
	{
		public string ResidentName { get; set; }
		public string UserName { get; set; }
	}
}