﻿using System;

namespace sntbr.domain.Helpers
{
	public static class DateTimeExtensions
	{
		public static DateTime ToMoscowTime(this DateTime utcTime)
		{
			return utcTime.AddHours(4);
		}

	}
}