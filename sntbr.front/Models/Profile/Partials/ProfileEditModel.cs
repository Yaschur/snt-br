﻿using System.ComponentModel.DataAnnotations;

namespace sntbr.front.Models.Profile.Partials
{
	public class ProfileEditModel
	{
		[Display(Name = "фамилия")]
		public string LastName { get; set; }

		[Display(Name = "имя")]
		public string FirstName { get; set; }

		[Display(Name = "отчество")]
		public string MiddleName { get; set; }

		[Required(ErrorMessage = "укажите адрес эл.почты!")]
		[EmailAddress(ErrorMessage = "неверный адрес эл.почты")]
		[Display(Name = "эл.почта")]
		public string Email { get; set; }
	}
}