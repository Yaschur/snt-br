﻿using sntbr.front.Models.Widgets;

namespace sntbr.front.Models
{
	public abstract class PageFactoryBase
	{
		public PageFactoryBase(WidgetFactory widgetFactory)
		{
			_widgetFactory = widgetFactory;
		}

		protected WidgetFactory _widgetFactory;
	}
}