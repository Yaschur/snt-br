﻿
namespace sntbr.front.Models.Housing.Pages
{
	public class EditPage : PageBaseViewModel
	{
		public string Number { get; private set; }

		public EditPage(string number)
			: base()
		{
			Number = number;
		}
	}
}