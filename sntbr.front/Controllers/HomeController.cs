﻿using System.Web.Mvc;
using sntbr.front.Models.Home;
using sntbr.front.Models.Home.Pages;

namespace sntbr.front.Controllers
{
	public class HomeController : Controller
	{
		public HomeController(HomePageFactory pageFactory)
		{
			_pageFactory = pageFactory;
		}

		public ActionResult Index()
		{
			IndexPage pageModel = _pageFactory.IndexPage();
			return View("~/Views/Home/Pages/Index.cshtml", pageModel);
		}

		private readonly HomePageFactory _pageFactory;
	}
}