﻿using Microsoft.Owin;
using Owin;
using sntbr.front.App_Start;

[assembly: OwinStartup(typeof(sntbr.front.Startup))]
namespace sntbr.front
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			OwinConfig.Configure(app);
		}
	}
}