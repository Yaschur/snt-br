﻿using System;
using NUnit.Framework;
using sntbr.domain.Publications;

namespace sntbr.test.Domain
{
	[TestFixture, Category("Domain")]
	public class ArticleShould
	{
		[Test]
		public void BeCreatedByIdTitleAndStory()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString();

			// Act
			var article = new Article(id, t, s);

			// Assert
			Assert.NotNull(article);
			Assert.AreEqual(id, article.Id);
			Assert.AreEqual(t, article.Title);
			Assert.AreEqual(s, article.Story);
		}

		[Test]
		public void CanChangeTitleAndStory()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(), t1 = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString(), s1 = Guid.NewGuid().ToString();
			var article = new Article(id, t, s);

			// Act
			article.SetStory(t1, s1);

			// Assert
			Assert.AreEqual(t1, article.Title);
			Assert.AreEqual(s1, article.Story);
		}

		[Test]
		public void SetCreatedAtTimestamp()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString();
			DateTime evTime = DateTime.UtcNow;

			// Act
			var article = new Article(id, t, s);
			int diff = Math.Abs((article.CreatedAt - evTime).Seconds);

			// Assert
			Assert.AreEqual(0, diff);
		}

		[TestCase(null, "notempty", "notempty")]
		[TestCase("", "notempty", "notempty")]
		[TestCase("notempty", null, "notempty")]
		[TestCase("notempty", "", "notempty")]
		[TestCase("notempty", "notempty", null)]
		[TestCase("notempty", "notempty", "")]
		public void ThrowIfCreatedOrChangedWithEmpties(string id, string t, string s)
		{
			// Arrange
			var article = new Article("note", "note", "note");

			// Act
			// Assert
			Assert.Throws<ArgumentException>(() => new Article(id, t, s));
			if (string.IsNullOrEmpty(id))
				return;
			Assert.Throws<ArgumentException>(() => article.SetStory(t, s));
		}

		[Test]
		public void BeUnpublishedAfterCreate()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString();

			// Act
			var article = new Article(id, t, s);

			// Assert
			Assert.IsFalse(article.IsPublished);
		}

		[Test]
		public void CanBePublished()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString();
			var article = new Article(id, t, s);

			// Act
			article.Publish();

			// Assert
			Assert.IsTrue(article.IsPublished);
		}

		[Test]
		public void CanBeUnpublished()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString();
			var article = new Article(id, t, s);
			article.Publish();

			// Act
			article.Publish(publish: false);

			// Assert
			Assert.IsFalse(article.IsPublished);
		}

		[Test]
		public void SupportTimestampsOnActions()
		{
			// Arrange
			string
				id = Guid.NewGuid().ToString(),
				t = Guid.NewGuid().ToString(), t1 = Guid.NewGuid().ToString(),
				s = Guid.NewGuid().ToString(), s1 = Guid.NewGuid().ToString();
			var article = new Article(id, t, s);
			DateTime evTime1 = DateTime.UtcNow;
			article.Publish();
			DateTime evTime2 = DateTime.UtcNow;
			article.SetStory(t1, s1);

			// Act
			int diff1 = Math.Abs((article.PublishedAt - evTime1).Seconds);
			int diff2 = Math.Abs((article.ChangedAt - evTime2).Seconds);

			// Assert
			Assert.AreEqual(0, diff1);
			Assert.AreEqual(0, diff2);
		}
	}
}
