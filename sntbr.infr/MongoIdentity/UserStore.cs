﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace sntbr.infr.MongoIdentity
{
	public class UserStore<TUser> :
		IUserStore<TUser>,
		IUserLoginStore<TUser>,
		IUserRoleStore<TUser>,
		IQueryableUserStore<TUser>
		where TUser : IdentityUser
	{
		public UserStore(string connectionString)
		{
			MongoUrl mUrl = new MongoUrl(connectionString);
			MongoClient mClient = new MongoClient(mUrl);
			MongoServer server = mClient.GetServer();
			_db = server.GetDatabase(mUrl.DatabaseName);
		}

		public Task AddLoginAsync(TUser user, UserLoginInfo login)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			if (!user.Logins.Any(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey))
			{
				user.Logins.Add(login);
			}

			return Task.FromResult(true);
		}

		public Task<TUser> FindAsync(UserLoginInfo login)
		{
			TUser user = _db.GetCollection<TUser>(_CollectionName).AsQueryable()
				.FirstOrDefault(u => u.Logins.Any(
					l => l.LoginProvider == login.LoginProvider
					&& l.ProviderKey == login.ProviderKey)
				);
			return Task.FromResult(user);
		}

		public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			return Task.FromResult((IList<UserLoginInfo>)user.Logins.ToList());
		}

		public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			user.Logins.RemoveAll(l => l.LoginProvider == login.LoginProvider
				&& l.ProviderKey == login.ProviderKey);

			return Task.FromResult(0);
		}

		public Task CreateAsync(TUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			_db.GetCollection<TUser>(_CollectionName).Insert(user);

			return Task.FromResult(user);
		}

		public Task DeleteAsync(TUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			_db.GetCollection(_CollectionName).Remove((Query.EQ("_id", ObjectId.Parse(user.Id))));

			return Task.FromResult(true);
		}

		public Task<TUser> FindByIdAsync(string userId)
		{
			TUser user = _db.GetCollection<TUser>(_CollectionName).AsQueryable()
				.FirstOrDefault(u => u.Id == userId);

			return Task.FromResult(user);
		}

		public Task<TUser> FindByNameAsync(string userName)
		{
			TUser user = _db.GetCollection<TUser>(_CollectionName).AsQueryable()
				.FirstOrDefault(u => u.UserName == userName);

			return Task.FromResult(user);
		}

		public Task UpdateAsync(TUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			var collection = _db.GetCollection<TUser>(_CollectionName);
			if (collection.AsQueryable().Any(u => u.Id == user.Id))
				collection.Save(user);

			return Task.FromResult(user);
		}

		public Task AddToRoleAsync(TUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			if (!user.Roles.Contains(roleName))
				user.Roles.Add(roleName);

			return Task.FromResult(true);
		}

		public Task<IList<string>> GetRolesAsync(TUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			return Task.FromResult<IList<string>>(user.Roles);
		}

		public Task<bool> IsInRoleAsync(TUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			return Task.FromResult(user.Roles.Contains(roleName));
		}

		public Task RemoveFromRoleAsync(TUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			user.Roles.RemoveAll(r => r == roleName);

			return Task.FromResult(0);
		}

		public IQueryable<TUser> Users
		{
			get
			{
				return _db.GetCollection<TUser>(_CollectionName).AsQueryable();
			}
		}

		public void Dispose()
		{
			return;
		}

		private readonly MongoDatabase _db;
		private const string _CollectionName = "IdentityUser";


	}
}
