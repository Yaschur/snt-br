﻿using System.Net;
using System.Web.Mvc;
using sntbr.front.Models.Common;
using sntbr.front.Models.Manage;
using sntbr.front.Models.Manage.Pages;
using sntbr.front.Models.Manage.Partials;

namespace sntbr.front.Controllers
{
	[Authorize(Roles = SntbrRoles.Administrator)]
	public class ManageController : Controller
	{
		public ManageController(ManagePageFactory pageFactory, ManageInputService inputService)
		{
			_pageFactory = pageFactory;
			_inputSerice = inputService;
		}

		public ActionResult Index()
		{
			IndexPage pageModel = _pageFactory.IndexPage();
			return View("~/Views/Manage/Pages/Index.cshtml", pageModel);
		}

		public ActionResult Create()
		{
			EditPage pageModel = _pageFactory.EditPage();
			return View("~/Views/Manage/Pages/Edit.cshtml", pageModel);
		}

		[HttpPost]
		public ActionResult Create(ArticleEditModel model)
		{
			if (!ModelState.IsValid)
			{
				EditPage pageModel = _pageFactory.EditPage();
				return View("~/Views/Manage/Pages/Edit.cshtml", pageModel);
			}
			_inputSerice.ProcessCreate(model);
			return RedirectToAction("Index");
		}

		public ActionResult Edit(string id)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			EditPage pageModel = _pageFactory.EditPage(id);
			if (pageModel == null)
				return new HttpStatusCodeResult(HttpStatusCode.NotFound);

			return View("~/Views/Manage/Pages/Edit.cshtml", pageModel);
		}

		[HttpPost]
		public ActionResult Edit(string id, ArticleEditModel model)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			if (!ModelState.IsValid)
			{
				EditPage pageModel = _pageFactory.EditPage(model);
				return View("~/Views/Manage/Pages/Edit.cshtml", pageModel);
			}
			_inputSerice.ProcessEdit(model, id);
			return RedirectToAction("Index");
		}

		public ActionResult Publish(string id)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			_inputSerice.ProcessPublish(id);
			return RedirectToAction("Index");
		}

		public ActionResult Delete(string id)
		{
			if (string.IsNullOrEmpty(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			_inputSerice.ProcessDelete(id);
			return RedirectToAction("Index");
		}

		private readonly ManagePageFactory _pageFactory;
		private readonly ManageInputService _inputSerice;
	}
}