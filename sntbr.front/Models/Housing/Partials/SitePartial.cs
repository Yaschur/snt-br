﻿using System.Collections.Generic;

namespace sntbr.front.Models.Housing.Partials
{
	public class SitePartial
	{
		public string Id { get; private set; }
		public string SiteNumber { get; private set; }
		public IEnumerable<ResidentInSitePartial> Residents { get; private set; }

		public SitePartial(string id, string siteNumber, IEnumerable<ResidentInSitePartial> residents)
		{
			Id = id;
			SiteNumber = siteNumber;
			Residents = residents;
		}
	}
}