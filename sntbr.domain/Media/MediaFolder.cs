﻿using System.Collections.Generic;
using System.IO;

namespace sntbr.domain.Media
{
	public class MediaFolder
	{
		public MediaFolder(string basePath)
		{
			_basePath = basePath;
		}

		public void Fold(Stream fileContent, string fileName)
		{
			if (fileContent == null || string.IsNullOrEmpty(fileName))
				return;

			string path = Path.Combine(_basePath, fileName);
			using (FileStream fs = new FileStream(path, FileMode.Create))
			{
				fileContent.CopyTo(fs);
			}
		}

		public string GetPath(string fileName)
		{
			string path = Path.Combine(_basePath, fileName);
			if (!File.Exists(path))
				return null;
			return path;
		}

		public IEnumerable<string> List()
		{
			return Directory.EnumerateFiles(_basePath);
		}

		public void Unfold(string name)
		{
			File.Delete(Path.Combine(_basePath, name));
		}

		private readonly string _basePath;
	}
}
