﻿using System;
using System.Collections.Generic;
using sntbr.front.Models.Account.Partials;

namespace sntbr.front.Models.Widgets
{
	public class AuthWidgetLogin : IWidgetViewModel
	{
		public IEnumerable<ProviderPartial> Providers { get; private set; }
		public string ReturnUrl { get; private set; }

		public string PartialView
		{
			get { return "~/Views/Widgets/AuthWidgetLogin.cshtml"; }
		}

		public AuthWidgetLogin(IEnumerable<ProviderPartial> providers, string returnUrl)
		{
			if (providers == null)
				throw new ArgumentNullException("providers");
			Providers = providers;
			ReturnUrl = returnUrl;
		}
	}
}