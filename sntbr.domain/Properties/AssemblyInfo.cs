﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("sntbr.domain")]
[assembly: AssemblyDescription("Domain model of SntBr")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d9fe087d-5714-4e41-99b8-24e0a0b6c759")]
