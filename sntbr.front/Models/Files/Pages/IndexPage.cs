﻿using System.Collections.Generic;
using sntbr.front.Models.Files.Partials;

namespace sntbr.front.Models.Files.Pages
{
	public class IndexPage : PageBaseViewModel
	{
		public IEnumerable<FilePartial> Files { get; private set; }

		public IndexPage(IEnumerable<FilePartial> files)
		{
			Files = files;
		}
	}
}