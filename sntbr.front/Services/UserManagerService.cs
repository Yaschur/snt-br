﻿using Microsoft.AspNet.Identity;
using sntbr.front.Models.Account;

namespace sntbr.front.Services
{
	public class UserManagerService : UserManager<AppUser>
	{
		public UserManagerService(IUserStore<AppUser> store)
			: base(store)
		{
			UserValidator = new UserValidator<AppUser>(this)
			{
				AllowOnlyAlphanumericUserNames = false
			};
		}
	}
}