﻿using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using sntbr.front.Models.Configuration;
using sntbr.front.Models.Account;
using sntbr.front.Services;
using sntbr.infr;
using sntbr.infr.MongoIdentity;
using sntbr.infr.MongoStore;

namespace sntbr.front.AppBuild.CastleInstallers
{
	public class ServicesInstaller : IWindsorInstaller
	{

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			string connString = SntbrSettings.Configuration.ConnectionString;

			// IStore
			container.Register(
				Component.For<IStore>()
				.ImplementedBy<MongoStore>()
				.DependsOn(new { connectionString = connString })
				.LifestyleSingleton()
			);

			// UserStore
			container.Register(
				Component.For<IUserStore<AppUser>>()
				.ImplementedBy<UserStore<AppUser>>()
				.DependsOn(new { connectionString = connString })
				.LifestylePerWebRequest()
			);

			// UserManagerService
			container.Register(
				Component.For<UserManagerService>()
				.LifestylePerWebRequest()
			);

			// IAuthenticationManager
			container.Register(
				Component.For<IAuthenticationManager>()
				.UsingFactoryMethod((k, c) => HttpContext.Current.GetOwinContext().Authentication)
				.LifestylePerWebRequest()
			);

			// All repositories
			container.Register(
				Classes.FromAssemblyNamed("sntbr.domain")
				.Where(t => t.Name.EndsWith("Repository"))
				.LifestylePerWebRequest()
			);
		}
	}
}