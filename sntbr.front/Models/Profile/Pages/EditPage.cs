﻿using sntbr.front.Models.Common;
using sntbr.front.Models.Profile.Partials;

namespace sntbr.front.Models.Profile.Pages
{
	public class EditPage : PageBaseViewModel
	{
		public ProfileEditModel ProfileInput { get; private set; }

		public EditPage(ProfileEditModel profileInput)
			: base(Constants.GeneralTitle, Constants.GeneralDescription)
		{
			ProfileInput = profileInput;
		}
	}
}