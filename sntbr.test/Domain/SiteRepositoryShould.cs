﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using NUnit.Framework;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;
using sntbr.infr.MongoStore;

namespace sntbr.test.Domain
{
	[TestFixture, Category("Domain"), Ignore]
	public class SiteRepositoryShould
	{
		SiteRepository repoUT;

		const string ConnectionString = "mongodb://localhost:27017/test";

		[SetUp]
		public void Setup()
		{
			repoUT = new SiteRepository(new MongoStore(ConnectionString));
		}

		[TearDown]
		public void TearDown()
		{
			MongoUrl mUrl = new MongoUrl(ConnectionString);
			MongoClient mClient = new MongoClient(mUrl);
			MongoServer mServer = mClient.GetServer();
			MongoDatabase mDb = mServer.GetDatabase(mUrl.DatabaseName);

			mDb.Drop();
		}

		[Test]
		public void KeepResidence()
		{
			// Arrange
			Resident
				resident1 = new Resident(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString()),
				resident2 = new Resident(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString()),
				resident3 = new Resident(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
			Site site = new Site(Guid.NewGuid().ToString(), Guid.NewGuid().ToString());

			repoUT.Store(site);

			// Act
			// Assert
			Site s1 = repoUT.GetById(site.Id);
			Assert.IsFalse(s1.HasResidence(resident1) && s1.HasResidence(resident2) && s1.HasResidence(resident3));

			s1.AddResidence(resident1);
			s1.AddResidence(resident2);
			repoUT.Store(s1);

			Site s2 = repoUT.GetById(site.Id);
			Assert.IsTrue(s2.HasResidence(resident1) && s2.HasResidence(resident2));
			Assert.IsFalse(s2.HasResidence(resident3));

			s2.AddResidence(resident3);
			s2.RemoveResidence(resident1);
			repoUT.Store(s2);
			
			Site s3 = repoUT.GetById(site.Id);
			Assert.IsTrue(s3.HasResidence(resident2) && s3.HasResidence(resident3));
			Assert.IsFalse(s3.HasResidence(resident1));			
		}
	}
}
