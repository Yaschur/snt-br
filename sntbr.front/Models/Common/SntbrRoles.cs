﻿
namespace sntbr.front.Models.Common
{
	public class SntbrRoles
	{
		public const string Administrator = "Administrator";
		public const string User = "User";

		public static string[] Roles = new string[] { SntbrRoles.User, SntbrRoles.Administrator };
	}
}