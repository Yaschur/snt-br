﻿using System;
using System.Linq.Expressions;

namespace sntbr.infr
{
	public interface IStoreMapper<TClass>
	{
		void SetMap<TDerived>();
		void SetMap<TProp>(Expression<Func<TClass, TProp>> lambdaExpression);
		void SetMap(string fieldName);
		void SetIgnore<TProp>(Expression<Func<TClass, TProp>> lambdaExpression);
		void SetKey<TProp>(Expression<Func<TClass, TProp>> lambdaExpression);
		bool AlreadyMapped { get; }
	}
}
