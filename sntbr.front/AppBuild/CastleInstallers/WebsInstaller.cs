﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using sntbr.front.Models;

namespace sntbr.front.AppBuild.CastleInstallers
{
	public class WebInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(
				Classes.FromThisAssembly()
				.BasedOn<IPageService>()
				.LifestylePerWebRequest()
			);

			container.Register(
				Classes.FromThisAssembly()
				.BasedOn<Controller>()
				.LifestylePerWebRequest()
			);
		}
	}
}