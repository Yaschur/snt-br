﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using sntbr.domain.Helpers;
using sntbr.domain.Media;
using sntbr.domain.Media.Repositories;
using sntbr.front.Models.Files.Partials;

namespace sntbr.front.Models.Files
{
	public class FilesInputService : IPageService
	{
		public FilesInputService(MediaFileRepository mediaFileRepository)
		{
			_mediaFileRepository = mediaFileRepository;

			// TODO: to review/ refactor
			string rootPath = HostingEnvironment.ApplicationPhysicalPath;
			_mediaFolder = new MediaFolder(
				Path.Combine(rootPath, FilesPageFactory.ContentFolderPath)
			);
		}

		public void ProcessUpload(HttpPostedFileBase fileInput, FileEditPartial descriptionInput)
		{
			if (fileInput == null || descriptionInput == null)
				return;
			string originalFileName = fileInput.FileName;
			string originalFileExtension = Path.GetExtension(originalFileName);
			string newFileName = string.Format(
				"{0}{1}",
				RandomStringGenerator.GetRandomString(8),
				originalFileExtension
			);
			_mediaFolder.Fold(fileInput.InputStream, newFileName);

			string foldedFilePath = _mediaFolder.GetPath(newFileName);
			if (string.IsNullOrEmpty(foldedFilePath))
				return;

			ImageFileBuilder imgBuilder = new ImageFileBuilder();
			MediaFile mediaFile = imgBuilder.IsKnown(foldedFilePath) ? imgBuilder.CreateMediaFile(foldedFilePath)
				: new MediaFile(newFileName);
			
			mediaFile.Rename(originalFileName, renameOriginalName: true);
			mediaFile.Describe(descriptionInput.Title ?? "noName", descriptionInput.Description);
			_mediaFileRepository.Store(mediaFile);
		}

		private readonly MediaFolder _mediaFolder;
		private readonly MediaFileRepository _mediaFileRepository;
	}
}