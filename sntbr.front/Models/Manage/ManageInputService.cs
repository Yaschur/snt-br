﻿using sntbr.domain.Helpers;
using sntbr.domain.Publications;
using sntbr.domain.Publications.Repositories;
using sntbr.front.Models.Manage.Partials;

namespace sntbr.front.Models.Manage
{
	public class ManageInputService : IPageService
	{
		public ManageInputService(ArticleRepository articleRepository)
		{
			_articleRepository = articleRepository;
		}

		public void ProcessCreate(ArticleEditModel input)
		{
			if (input == null)
				return;
			Article article = new Article(RandomStringGenerator.GetRandomString(8), input.Title, input.Story);
			_articleRepository.Store(article);
		}

		public void ProcessEdit(ArticleEditModel input, string id)
		{
			if (input == null || string.IsNullOrEmpty(id))
				return;
			Article article = _articleRepository.GetById(id);
			if (article == null)
				return;
			article.SetStory(input.Title, input.Story);
			_articleRepository.Store(article);
		}

		public void ProcessPublish(string id)
		{
			Article article = _articleRepository.GetById(id);
			if (article == null)
				return;
			article.Publish(!article.IsPublished);
			_articleRepository.Store(article);
		}

		public void ProcessDelete(string id)
		{
			Article article = _articleRepository.GetById(id);
			if (article == null)
				return;
			_articleRepository.Remove(article);
		}

		private readonly ArticleRepository _articleRepository;
	}
}