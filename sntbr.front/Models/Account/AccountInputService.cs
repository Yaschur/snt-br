﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using sntbr.domain.Helpers;
using sntbr.front.Models.Common;
using sntbr.front.Services;

namespace sntbr.front.Models.Account
{
	public class AccountInputService : IPageService
	{
		public AccountInputService(IAuthenticationManager authenticationManager, UserManagerService userManager)
		{
			_authManager = authenticationManager;
			_userManager = userManager;
		}

		public void ProcessLogoff()
		{
			_authManager.SignOut();
		}

		public void ProcessExternalCallBackAsync(bool isPersistent, out IEnumerable<string> errors)
		{
			List<string> ers = new List<string>();
			errors = ers;

			ExternalLoginInfo loginInfo = _authManager.GetExternalLoginInfo();
			if (loginInfo == null)
			{
				ers.Add("Invalid request to callback...");
				return;
			}

			AppUser user = _userManager.FindAsync(loginInfo.Login).Result;
			if (user != null)
			{
				SignIn(user, isPersistent);
				return;
			}

			bool firstLogin = !_userManager.Users.Any();

			string userName = GetNewUserName(loginInfo);
			user = new AppUser() { UserName = userName, Email = loginInfo.Email };
			IdentityResult result = _userManager.Create(user);
			if (!result.Succeeded)
			{
				ers.AddRange(result.Errors);
				return;
			}

			result = _userManager.AddLogin(user.Id, loginInfo.Login);
			if (!result.Succeeded)
			{
				ers.AddRange(result.Errors);
				return;
			}

			result = _userManager.AddToRole(user.Id, SntbrRoles.User);
			if (!result.Succeeded)
			{
				ers.AddRange(result.Errors);
				return;
			}

			if (firstLogin)
			{
				result = _userManager.AddToRole(user.Id, SntbrRoles.Administrator);
				if (!result.Succeeded)
				{
					ers.AddRange(result.Errors);
					return;
				}
			}

			SignIn(user, isPersistent);
		}

		public void ProcessDetailsEdit(string id, string[] inRoles)
		{
			AppUser user = _userManager.FindById(id);
			if (user == null)
				return;

			bool notEmpty = inRoles != null;
			foreach (var role in SntbrRoles.Roles)
			{
				if (notEmpty && inRoles.Contains(role))
					_userManager.AddToRole(id, role);
				else
					_userManager.RemoveFromRole(id, role);
			}
		}


		private string GetNewUserName(ExternalLoginInfo loginInfo)
		{
			string name = loginInfo.DefaultUserName;
			int i;
			for (i = 0; _userManager.FindByName(name) != null && i < 1000; i++)
			{
				name = string.Format("{0}_{1}", loginInfo.DefaultUserName, RandomStringGenerator.GetRandomString(4));
			}
			if (i == 1000)
				name = string.Format("{0}_{1}", loginInfo.DefaultUserName, Guid.NewGuid().ToString());
			return name;
		}

		private void SignIn(AppUser user, bool isPersistent)
		{
			_authManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
			_authManager.SignIn(
				new AuthenticationProperties() { IsPersistent = isPersistent },
				_userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie));
		}

		private readonly IAuthenticationManager _authManager;
		private readonly UserManagerService _userManager;
	}
}