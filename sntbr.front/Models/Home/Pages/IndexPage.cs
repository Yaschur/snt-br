﻿using System.Collections.Generic;
using sntbr.front.Models.Home.Partials;

namespace sntbr.front.Models.Home.Pages
{
	public class IndexPage : PageBaseViewModel
	{
		public IEnumerable<ArticlePartial> Articles { get; private set; }

		public IndexPage(IEnumerable<ArticlePartial> articles)
			: base()
		{
			Articles = articles;
		}
	}
}