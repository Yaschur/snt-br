﻿using System;
using System.Collections.Generic;

namespace sntbr.domain.Housing
{
	public class Site
	{
		public string Id { get; private set; }

		public string Number { get; set; }

		public Site(string id, string number)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");
			Id = id;
			if (string.IsNullOrEmpty(number))
				throw new ArgumentException("number cannot be null or empty");
			Number = number;
			_residents = new List<string>();
		}

		public void AddResidence(Resident resident)
		{
			if (HasResidence(resident))
				return;
			_residents.Add(resident.Id);
		}

		public void RemoveResidence(Resident resident)
		{
			if (!HasResidence(resident))
				return;
			_residents.Remove(resident.Id);
		}

		public bool HasResidence(string residentId)
		{
			return _residents.Contains(residentId);
		}

		public bool HasResidence(Resident resident)
		{
			return HasResidence(resident.Id);
		}

		private List<string> _residents;
	}
}
