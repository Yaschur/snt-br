﻿using System.Collections.Generic;

namespace sntbr.front.Models.Account.Pages
{
	public class UsersPage : PageBaseViewModel
	{
		public IEnumerable<AppUser> Users { get; private set; }

		public UsersPage(IEnumerable<AppUser> users)
			: base()
		{
			Users = users;
		}
	}
}