﻿using sntbr.domain.Helpers;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;

namespace sntbr.front.Models.Housing
{
	public class HousingInputService : IPageService
	{
		public HousingInputService(SiteRepository siteRepository)
		{
			_siteRepository = siteRepository;
		}

		public void ProcessCreate(string input)
		{
			string clearedInput = (input ?? "").Trim();
			if (string.IsNullOrEmpty(clearedInput) || _siteRepository.GetByNumber(clearedInput) != null)
				return;
			Site site = new Site(RandomStringGenerator.GetRandomString(8), clearedInput);
			_siteRepository.Store(site);
		}

		public void ProcessEdit(string input, string id)
		{
			string clearedInput = (input ?? "").Trim();
			if (input == null || string.IsNullOrEmpty(id) || _siteRepository.GetByNumber(clearedInput) != null)
				return;
			Site site = _siteRepository.GetById(id);
			if (site == null)
				return;
			site.Number = clearedInput;
			_siteRepository.Store(site);
		}

		public void ProcessDelete(string id)
		{
			Site site = _siteRepository.GetById(id);
			if (site == null)
				return;
			_siteRepository.Remove(site);
		}

		private readonly SiteRepository _siteRepository;
	}
}