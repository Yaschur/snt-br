﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Windsor;

namespace sntbr.front.AppBuild
{
	public class CastleDependencyResolver : IDependencyResolver
	{
		public CastleDependencyResolver(IWindsorContainer container)
		{
			_container = container;
		}

		public object GetService(Type serviceType)
		{
			return _container.Kernel.HasComponent(serviceType) ?
				_container.Resolve(serviceType) : null;
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return _container.Kernel.HasComponent(serviceType) ?
				_container.ResolveAll(serviceType).Cast<object>()
				: Enumerable.Empty<object>();
		}

		private readonly IWindsorContainer _container;
	}
}