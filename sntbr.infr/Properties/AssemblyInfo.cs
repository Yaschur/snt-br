﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("sntbr.infr")]
[assembly: AssemblyDescription("Support level of SntBr apps")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a4e7318d-8080-4051-8d4e-a03c01e7f10d")]
