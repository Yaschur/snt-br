﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using sntbr.front.Models.Account.Pages;
using sntbr.front.Models.Account.Partial;
using sntbr.front.Models.Account.Partials;
using sntbr.front.Models.Common;
using sntbr.front.Services;

namespace sntbr.front.Models.Account
{
	public class AccountPageFactory : IPageService
	{
		public AccountPageFactory(IAuthenticationManager authenticationManager, UserManagerService userManager)
		{
			_authManager = authenticationManager;
			_userManager = userManager;
		}

		public LoginPage LoginPage(string returnUrl)
		{
			ProviderPartial[] providers = _authManager.ProviderPartials();
			LoginPage page = new LoginPage(providers, returnUrl);

			return page;
		}

		public UsersPage UsersPage()
		{
			List<AppUser> users = _userManager.Users
				.OrderBy(u => u.UserName)
				.ToList();

			UsersPage page = new UsersPage(users);

			return page;
		}

		public DetailsPage DetailsPage(string id)
		{
			AppUser user = _userManager.FindById(id);
			if (user == null)
				return null;
			AppUserEditModel model = new AppUserEditModel
			{
				Email = user.Email,
				InRoles = user.Roles.ToArray(),
				UserName = user.UserName
			};
			return new DetailsPage(model, SntbrRoles.Roles);
		}

		public ChallengeResult ExternalLogin(string provider, string returnUri)
		{
			return new ChallengeResult(provider, returnUri, _authManager);
		}

		private readonly IAuthenticationManager _authManager;
		private readonly UserManagerService _userManager;
		private const string XsrfKey = "XsrfId";

		public class ChallengeResult : HttpUnauthorizedResult
		{
			public ChallengeResult(string provider, string redirectUri, IAuthenticationManager authManager)
				: this(provider, redirectUri, null, authManager)
			{
			}

			public ChallengeResult(string provider, string redirectUri, string userId, IAuthenticationManager authManager)
			{
				LoginProvider = provider;
				RedirectUri = redirectUri;
				UserId = userId;
				_authManager = authManager;
			}

			public string LoginProvider { get; set; }
			public string RedirectUri { get; set; }
			public string UserId { get; set; }

			public override void ExecuteResult(ControllerContext context)
			{
				var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
				if (UserId != null)
				{
					properties.Dictionary[XsrfKey] = UserId;
				}
				_authManager.Challenge(properties, LoginProvider);
			}

			private IAuthenticationManager _authManager;
		}

	}
}