﻿using System.Linq;
using Microsoft.Owin.Security;
using sntbr.front.Models.Account.Partials;

namespace sntbr.front.Models.Common
{
	public static class AuthenticationManagerExtension
	{
		public static ProviderPartial[] ProviderPartials(this IAuthenticationManager authManager)
		{
			return authManager.GetExternalAuthenticationTypes()
				.Select(eat => new ProviderPartial
				{
					AuthenticationType = eat.AuthenticationType,
					Caption = eat.Caption,
					LogoUrl = string.Format("{0}/{1}.png", Constants.ImagesBasePath, eat.AuthenticationType.ToLower())
				})
				.ToArray();
		}
	}
}