﻿using System.Collections.Generic;
using sntbr.front.Models.Housing.Partials;

namespace sntbr.front.Models.Housing.Pages
{
	public class IndexPage : PageBaseViewModel
	{
		public IEnumerable<SitePartial> Sites { get; private set; }

		public IndexPage(IEnumerable<SitePartial> sites)
			: base()
		{
			Sites = sites;
		}
	}
}