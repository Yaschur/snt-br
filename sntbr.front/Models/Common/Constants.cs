﻿namespace sntbr.front.Models.Common
{
	public static class Constants
	{
		public const string GeneralTitle = "СНТ Березовая Роща, Домодедовский район, Московская область";
		public const string GeneralDescription = "СНТ Березовая Роща, Домодедовский район, Московская область";

		public const string ImagesBasePath = "/content/img";
	}
}