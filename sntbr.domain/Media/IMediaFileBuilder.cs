﻿namespace sntbr.domain.Media
{
	public interface IMediaFileBuilder
	{
		bool IsKnown(string filePath);
		MediaFile CreateMediaFile(string filePath);
	}
}
