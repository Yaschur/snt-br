﻿
namespace sntbr.front.Models.Profile.Partials
{
	public class SiteEditPartial
	{
		public string SiteId { get; set; }
		public string SiteNumber { get; set; }
		public string Action { get; set; }
		public string ActionTitle { get; set; }
	}
}