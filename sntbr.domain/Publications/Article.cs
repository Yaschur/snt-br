﻿using System;

namespace sntbr.domain.Publications
{
	public class Article
	{
		public string Id { get; private set; }
		public string Title { get; private set; }
		public string Story { get; private set; }
		public bool IsPublished { get; private set; }

		public DateTime CreatedAt { get; private set; }
		public DateTime ChangedAt { get; private set; }
		public DateTime PublishedAt { get; private set; }

		public Article(string id, string title, string story)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");
			Id = id;
			SetStory(title, story);
			CreatedAt = DateTime.UtcNow;
			ChangedAt = DateTime.UtcNow;
			PublishedAt = default(DateTime);
		}

		public void SetStory(string title, string story)
		{
			if (string.IsNullOrEmpty(title))
				throw new ArgumentException("title cannot be null or empty");
			if (string.IsNullOrEmpty(story))
				throw new ArgumentException("story cannot be null or empty");
			if (!title.Equals(Title) || !story.Equals(Story))
				ChangedAt = DateTime.UtcNow;

			Title = title;
			Story = story;
		}

		public void Publish(bool publish = true)
		{
			if (!IsPublished && publish)
				PublishedAt = DateTime.UtcNow;
			IsPublished = publish;
		}
	}
}
