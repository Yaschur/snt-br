﻿using sntbr.domain.Helpers;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;
using sntbr.front.Models.Account;
using sntbr.front.Models.Profile.Partials;
using sntbr.front.Services;

namespace sntbr.front.Models.Profile
{
	public class ProfileInputService : IPageService
	{
		public ProfileInputService(
			UserManagerService userManager,
			ResidentRepository residentRepository,
			SiteRepository siteRepository)
		{
			_userManager = userManager;
			_residentRepo = residentRepository;
			_siteRepo = siteRepository;
		}

		public void ProcessProfileEdit(string userName, ProfileEditModel input)
		{
			if (input == null)
				return;
			AppUser user = _userManager.FindByNameAsync(userName).Result;
			if (user == null)
				return;
			if (user.Email != input.Email)
				user.Email = input.Email;
			_userManager.UpdateAsync(user);

			Resident resident = _residentRepo.GetByUserName(userName);
			if (string.IsNullOrEmpty(input.LastName) && string.IsNullOrEmpty(input.FirstName))
				return;
			if (resident == null)
			{
				resident = new Resident(RandomStringGenerator.GetRandomString(8));
				resident.AssociateUser(userName);
			}
			resident.SetName(input.LastName, input.FirstName, input.MiddleName);
			_residentRepo.Store(resident);
		}

		public void ProcessSiteSet(string userName, string siteId, string action)
		{
			Resident resident = _residentRepo.GetByUserName(userName);
			if (resident == null)
				return;
			Site site = _siteRepo.GetById(siteId);
			if (site == null)
				return;
			bool needSave = false;
			if (action == ProfilePageFactory.ActionSiteAdd)
			{
				site.AddResidence(resident);
				needSave = true;
			}
			if (action == ProfilePageFactory.ActionSiteRemove)
			{
				site.RemoveResidence(resident);
				needSave = needSave || true;
			}
			if (needSave)
				_siteRepo.Store(site);

		}

		private readonly UserManagerService _userManager;
		private readonly ResidentRepository _residentRepo;
		private readonly SiteRepository _siteRepo;
	}
}