﻿using System.Web;
using Microsoft.Owin.Security;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;
using sntbr.front.Models.Common;

namespace sntbr.front.Models.Widgets
{
	public class WidgetFactory : IPageService
	{
		public WidgetFactory(IAuthenticationManager authenticationManager, ResidentRepository residentRepository)
		{
			_authManager = authenticationManager;
			_residentRepo = residentRepository;
		}

		public GisMeteoWidget GisMeteoWidget()
		{
			return new GisMeteoWidget("4369", "SeYST1CgdU7VcH");
		}

		public IWidgetViewModel AuthWidget()
		{
			if (!_authManager.User.Identity.IsAuthenticated)
			{
				var provs = _authManager.ProviderPartials();
				return new AuthWidgetLogin(provs, HttpContext.Current.Request.Url.ToString());
			}

			string userName = _authManager.User.Identity.Name;
			Resident resident = _residentRepo.GetByUserName(userName);
			return new AuthWidget(resident == null ? userName : resident.FirstName ?? resident.LastName);
		}

		private readonly IAuthenticationManager _authManager;
		private readonly ResidentRepository _residentRepo;
	}
}