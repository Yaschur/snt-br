﻿using sntbr.front.Models.Manage.Partials;

namespace sntbr.front.Models.Manage.Pages
{
	public class EditPage : PageBaseViewModel
	{
		public ArticleEditModel ArticleInput { get; private set; }

		public EditPage(ArticleEditModel input)
			: base()
		{
			ArticleInput = input;
		}
	}
}