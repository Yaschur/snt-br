﻿
using sntbr.front.Models.Files.Partials;
namespace sntbr.front.Models.Files.Pages
{
	public class UploadPage : PageBaseViewModel
	{
		public FileEditPartial FileDescription { get; private set; }

		public UploadPage(FileEditPartial fileDescription)
			: base()
		{
			FileDescription = fileDescription;
		}
	}
}