﻿using System;
using System.Linq;

namespace sntbr.infr
{
	public interface IStore : IDisposable
	{
		void Store<T>(T objectToSave);

		void Remove<T>(object id);

		//void Purge<T>(T objectToResetFromStore);

		IQueryable<T> AllOf<T>();

		IQueryable<TDerived> AllOf<TDerived, TParent>()
			where TDerived : TParent;

		IStoreMapper<T> GetMapper<T>();
	}
}
