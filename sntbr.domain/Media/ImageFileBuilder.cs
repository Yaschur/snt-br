﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace sntbr.domain.Media
{
	public class ImageFileBuilder : IMediaFileBuilder
	{
		public bool IsKnown(string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
				return false;

			return ValidExtensions.Contains(Path.GetExtension(filePath).ToLower());
		}

		public MediaFile CreateMediaFile(string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
				return null;

			string fileName = Path.GetFileName(filePath);
			ImageFile imgFile = new ImageFile(fileName);
			using (Image img = Image.FromFile(filePath))
			{
				imgFile.SetDimension(img.Width, img.Height);
			}

			return imgFile;
		}

		private string[] ValidExtensions = new string[] { ".jpg", ".jpeg", ".gif", ".png" };
	}
}
