﻿
namespace sntbr.front.Models.Widgets
{
	public class GisMeteoWidget : IWidgetViewModel
	{
		public string City { get; private set; }

		public string Hash { get; private set; }

		public string PartialView
		{
			get { return "~/Views/Widgets/GisMeteoWidget.cshtml"; }
		}

		public GisMeteoWidget(string city, string hash)
		{
			City = city;
			Hash = hash;
		}
	}
}