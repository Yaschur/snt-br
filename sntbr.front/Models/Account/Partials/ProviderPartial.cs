﻿
namespace sntbr.front.Models.Account.Partials
{
	public class ProviderPartial
	{
		public string AuthenticationType { get; set; }
		public string Caption { get; set; }
		public string LogoUrl { get; set; }
	}
}