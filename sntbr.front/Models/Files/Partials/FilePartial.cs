﻿
namespace sntbr.front.Models.Files.Partials
{
	public class FilePartial
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public string Url { get; set; }
	}
}