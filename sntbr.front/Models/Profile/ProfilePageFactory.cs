﻿using System.Linq;
using sntbr.domain.Housing;
using sntbr.domain.Housing.Repositories;
using sntbr.front.Models.Account;
using sntbr.front.Models.Profile.Pages;
using sntbr.front.Models.Profile.Partials;
using sntbr.front.Models.Widgets;
using sntbr.front.Services;

namespace sntbr.front.Models.Profile
{
	public class ProfilePageFactory : PageFactoryBase, IPageService
	{
		public const string ActionSiteAdd = "add";
		public const string ActionSiteRemove = "remove";
		public const string TitleSiteAdd = "поселиться";
		public const string TitleSiteRemove = "выехать";

		public ProfilePageFactory(
			UserManagerService userManager,
			ResidentRepository residentRepository,
			SiteRepository siteRepository,
			WidgetFactory widgetFactory)
			: base(widgetFactory)
		{
			_userManager = userManager;
			_residentRepo = residentRepository;
			_siteRepo = siteRepository;
		}

		public IndexPage IndexPage(string userName)
		{
			AppUser user = _userManager.FindByNameAsync(userName).Result;
			Resident resident = _residentRepo.GetByUserName(userName);
			Site[] sites = resident == null ? new Site[0] : _siteRepo.FindByResidence(resident.Id).ToArray();

			string fullName = resident == null ? string.Empty
				: string.Join(
					" ",
					(new[] { resident.LastName, resident.FirstName, resident.MiddleName })
					.Where(s => !string.IsNullOrEmpty(s))
				);

			IndexPage page = new IndexPage(
				fullName,
				user.Email,
				string.Join(", ", user.Roles),
				string.Join(", ", sites.Select(s => s.Number))
			);

			page.SetWidgets(
				_widgetFactory.GisMeteoWidget()
			);

			return page;
		}

		public EditPage EditPage(string userName)
		{
			AppUser user = _userManager.FindByNameAsync(userName).Result;
			Resident resident = _residentRepo.GetByUserName(userName);

			ProfileEditModel input = new ProfileEditModel
			{
				Email = user.Email
			};

			if (resident != null)
			{
				input.FirstName = resident.FirstName;
				input.LastName = resident.LastName;
				input.MiddleName = resident.MiddleName;
			}

			EditPage page = new EditPage(input);

			page.SetWidgets(
				_widgetFactory.GisMeteoWidget()
			);

			return page;
		}

		public EditPage EditPage(ProfileEditModel input)
		{
			EditPage page = new EditPage(input);

			page.SetWidgets(
				_widgetFactory.GisMeteoWidget()
			);

			return page;
		}

		public SitePage SitePage(string userName)
		{
			Resident resident = _residentRepo.GetByUserName(userName);
			if (resident == null)
				return null;
			//Site[] sites = _siteRepo.FindByResidence(resident.Id).ToArray();
			SiteEditPartial[] sites = _siteRepo.FindAll()
				.ToList()
				.OrderBy(s => s.Number.PadLeft(2, '0'))
				.Select(s => new SiteEditPartial
				{
					Action = s.HasResidence(resident) ? ActionSiteRemove : ActionSiteAdd,
					ActionTitle = s.HasResidence(resident) ? TitleSiteRemove : TitleSiteAdd,
					SiteId = s.Id,
					SiteNumber = s.Number
				})
				.ToArray();

			SitePage page = new SitePage(sites);

			return page;
		}

		private readonly UserManagerService _userManager;
		private readonly ResidentRepository _residentRepo;
		private readonly SiteRepository _siteRepo;
	}
}