﻿
namespace sntbr.domain.Media
{
	public class ImageFile : MediaFile
	{
		public int Width { get; private set; }
		public int Height { get; private set; }

		public ImageFile(string name) : base(name) { }

		public void SetDimension(int width, int height)
		{
			Width = width;
			Height = height;
		}
	}
}
