﻿using System.Web.Mvc;
using sntbr.front.Models.Common;
using sntbr.front.Models.Files;
using sntbr.front.Models.Files.Pages;
using sntbr.front.Models.Files.Partials;

namespace sntbr.front.Controllers
{
	[Authorize(Roles = SntbrRoles.Administrator)]
	public class FilesController : Controller
	{
		public FilesController(FilesPageFactory pageFactory, FilesInputService inputService)
		{
			_pageFactory = pageFactory;
			_inputService = inputService;
		}

		public ActionResult Index()
		{
			IndexPage pageModel = _pageFactory.IndexPage();
			return View("~/Views/Files/Pages/Index.cshtml", pageModel);
		}

		public ActionResult Upload()
		{
			UploadPage pageModel = _pageFactory.UploadPage();
			return View("~/Views/Files/Pages/Upload.cshtml", pageModel);
		}

		[HttpPost]
		public ActionResult Upload(FileEditPartial model)
		{
			_inputService.ProcessUpload(Request.Files[0], model);
			return RedirectToAction("index");
		}

		private readonly FilesPageFactory _pageFactory;
		private readonly FilesInputService _inputService;
	}
}